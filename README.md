# 搜狗输入法优麒麟社区版打包计划
这个版本是[搜狗输入法优麒麟打包计划](https://gitee.com/laomocode/fcitx-sogoupinyin)的后续版本，提取自Ubuntu Kylin 20.04 LTS。

从这个版本提取出来的搜狗输入法名字变了，后面加了优麒麟社区版，也变成企业定制版了。

好了，不多说，上截图：

![JBrtJ0.png](https://s1.ax1x.com/2020/04/24/JBrtJ0.png)

下载链接：
- [码云下载](https://gitee.com/laomocode/fcitx-sogouimebs/releases)